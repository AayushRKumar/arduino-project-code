 /* IR remote and receiver Arduino example code. Print key values in the Serial Monitor. More info: https://www.makerguides.com */
#include <IRremote.h> // include the IRremote library
#include <Servo.h>
#define RECEIVER_PIN 11 // define the IR receiver pin
IRrecv receiver(RECEIVER_PIN); // create a receiver object of the IRrecv class
decode_results results; // create a results object of the decode_results class
Servo servo1;
Servo servo2;

int wholeNoteDelay = 1600;
int servoTiltLeft = 180;
int servoTiltRight = 0;
int menu = 0;
unsigned long key_value = 0; // variable to store the key value
void setup() {
  Serial.begin(9600); // begin serial communication with a baud rate of 9600
  receiver.enableIRIn(); // enable the receiver
  receiver.blink13(true); // enable blinking of the built-in LED when an IR signal is received
  
  servo1.attach(9);

}
enum NoteLetter {c, d, e, f, g};


 

NoteLetter hotCrossBunsLetters[] = {e, d, c, e, d, c, e, e, e, e, d, d, d, d, e, d, c};
double hotCrossBunsLengths[] = {.25, .25, .5, .25, .25, .5, .125, .125, .125, .125, .125, .125, .125, .125, .25, .25, .5};

void loop() {
  int pos = 0;
//   for (pos = 50; pos <= 150; pos += 1) { // goes from 0 degrees to 180 degrees
//    // in steps of 1 degree
//    servo1.write(pos);              // tell servo to go to position in variable 'pos'
//    delay(15);                       // waits 15ms for the servo to reach the position
//  }
//  
 if (menu == 0) {
    //Prints the menu to the serial port
    Serial.println("Select an option:");
    Serial.println("-----------------");
    Serial.println("1) Change Servo-Tilt-Left");
    Serial.println("2) Change Servo-Tilt-Right");
   
  
    Serial.flush();

    while (!Serial.available()) {
      IR_sensor();
      }  //Waits for an input on the serial device
    menu = Serial.parseInt();       //Takes the Serial input and looks for an integer
    Serial.flush();
  }
  
  if (menu == 1) {
    
     Serial.println("Enter a value.");
      
      while (!Serial.available()) {}   
Serial.flush();
       int input = Serial.parseInt();  //Finds the input variable sent through serial
      Serial.print("Setting Servo-Tilt-Left to:");
      Serial.println(input);
      
      servoTiltLeft = input;
       menu = 0;
  }
   else if (menu == 2) {
    Serial.println("Enter a value.");
      
      while (!Serial.available()) {}   
      Serial.flush();
    int input = Serial.parseInt();  //Finds the input variable sent through serial
       Serial.print("Setting Servo-Tilt-Right to:");
      Serial.println(input);
      
      servoTiltRight = input;
      menu = 0;
  }
}

void IR_sensor() {
  if (receiver.decode()) {
  //  Serial.println(receiver.results.value, HEX);
  if (receiver.results.value == 0xFFFFFFFF) { // if the value is equal to 0xFFFFFFFF
      receiver.results.value = key_value; // set the value to the key value
    }
   
    switch (receiver.results.value) { // compare the value to the following cases
      case 0xFFA25D: // if the value is equal to 0xFD00FF
        Serial.println("POWER"); // print "POWER" in the Serial Monitor
        servo1.write(90);
        break;
      case 0xFF629D:
        Serial.println("VOL+");
//        servo1.write(270);
//         delay(250);
//          servo1.write(0);
       playSong("HotCrossBuns");
        break;
      case 0xFFE21D:
        Serial.println("FUNC/STOP");
        break;
      case 0xFD20DF:
        Serial.println("|<<");
        break;
      case 0xFDA05F:
        Serial.println(">||");
        break ;
      case 0xFD609F:
        Serial.println(">>|");
        break ;
      case 0xFD10EF:
        Serial.println("DOWN");
        break ;
      case 0xFD906F:
        Serial.println("VOL-");
        break ;
      case 0xFD50AF:
        Serial.println("UP");
        break ;
      case 0xFD30CF:
        Serial.println("0");
        break ;
      case 0xFDB04F:
        Serial.println("EQ");
        break ;
      case 0xFD708F:
        Serial.println("ST/REPT");
        break ;
      case 0xFF30CF:
      
      //  Serial.println("1");
        servo1.write(servoTiltLeft);
        delay(150);
          servo1.write(90);
         
          
        break ;
      case 0xFF18E7:
       // Serial.println("2");
//        servo1.write(map(servoTiltRight,0, 500, 255, 0));
servo2.attach(10);
delay(100);
    servo2.write(servoTiltLeft);  
    servo2.detach();
        delay(150);
       servo2.attach(10);
delay(100);
          servo2.write(90);
          
          servo2.detach();
        break ;
      case 0xFF7A85:
        Serial.println("3");
        break ;
      case 0xFF10EF:
        Serial.println("4");
        break ;
      case 0xFF38C7:
        Serial.println("5");
        break ;
      case 0xFF5AA5:
        Serial.println("6");
        break ;
      case 0xFF42BD:
        Serial.println("7");
        break ;
      case 0xFF4AB5:
        Serial.println("8");
        break ;
      case 0xFF52AD:
        Serial.println("9");
        break ;
    }
     key_value = results.value; // store the value as key_value
    receiver.resume();
  }

}
  void playSong(String song) {
    if (song == "HotCrossBuns") {
      double currLength;
      for (int i=0; i < 17; i++) {
        // 1 plays c & d
        //2 plays e & f
          NoteLetter currNote = hotCrossBunsLetters[i];
         currLength = hotCrossBunsLengths[i] * wholeNoteDelay;
          if  (hotCrossBunsLetters[i] == 0) {
             Serial.println("c");
          }
           if  (hotCrossBunsLetters[i] == 1) {
             Serial.println("d");
          }
          if  (hotCrossBunsLetters[i] == 2) {
             Serial.println("e");
          }
          
          switch (currNote) {
            case c:
            delay(100);
             servo1.write(servoTiltRight);  
        delay(currLength);
         delay(100);
          servo1.write(90);

             break;
            case d:
            delay(100);
             servo1.write(servoTiltLeft);  
        delay(currLength);
        delay(100);
          servo1.write(90);
         

          
            break;
          case e:
          Serial.print("SERVO2");
          servo2.attach(10);
          delay(100);
          servo2.write(servoTiltLeft);
           servo2.detach();
          delay(currLength);
           servo2.attach(10);
            delay(100);
          servo2.write(90);

          servo2.detach();
          break;
         default: 
             Serial.println("default");
           break;

          
          }
        delay(currLength);
      }
    }
  }
