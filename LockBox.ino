#include "LiquidCrystal.h" //Initializing the library for LCD
#include <Servo.h>
const int switchPin = 11;  // Joystick button
const int pinX = A2; // Joystick X axis
const int pinY = A3; // Joystick Y axis
const int ledPin = 13;
int dialogCount = 0;
int oldPos = 0;
int timeCutPercentage = 3;
Servo servo1;
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);
//MARK: Bytes

byte lcdFullBlock[8] = {
  B00100,
  B01110,
  B10101,
  B00100,
  B00100,
  B00100,
  B00100,
  B00100

};
byte lcdDownArrow[8] = {
B00100,
  B00100,
  B00100,
  B00100,
  B00100,
  B10101,
  B01110,
  B00100
};
byte lcdBackSpaceArrow[8] = {
  B00000,
  B00100,
  B01000,
  B11111,
  B11111,
  B01000,
  B00100,
  B00000
};

int lcdCursorPos = 8;
void setup() //Anything written in it will only run once
{
   servo1.attach(10);
   
    servo1.write(90);
//  delay(20000);
//  servo1.write(150);
lcd.begin(16, 2); //Initializing the interface on the LCD screen
lcd.createChar(0, lcdFullBlock); // create a new custom character
lcd.createChar(1, lcdDownArrow); // create a new custom character
lcd.createChar(2, lcdBackSpaceArrow); // create a new custom character
//delay(1000);
 
resetLCD();
dialogCount = 0;
  pinMode(ledPin, OUTPUT);
  pinMode(pinX, INPUT);
  pinMode(pinY, INPUT);
  pinMode(switchPin, INPUT);
  digitalWrite(switchPin, HIGH); // Connect the built-in wind-up resistor
  Serial.begin(9600);
}
bool buttonPressLock = false;
int writingSlot = 0;
int timeInSeconds = 0;
int wholeTime = 0;
int answerGiven = 0;
int answer = 0;

void resetLCD() {
lcd.setCursor(0, 0);// set the cursor to column 0, line1
lcd.print("Set Timer? Yes");//print name
lcd.setCursor(15, 0);// set the cursor to column 0, line15
lcd.write((byte)1);
lcd.setCursor(0, 1); // set the cursor to column 0, line 2
lcd.print("                       ");//print name
}  
bool neg = 0;
void loop() //Anything written in it will run again and again
{

  int ledState = digitalRead(switchPin); // Read the button's state
  if (ledState == 0) {
  if (!buttonPressLock) {
    buttonPressLock = true;
    //do action
    if (dialogCount == 0) {
      if (lcdCursorPos == 15) {
        dialogCount += 1;
      lcd.setCursor(0, 0);// set the cursor to column 0, line1
         lcd.print("                     ");
       lcd.print("Choose Time:");//print name
       delay(1000);
       writingSlot = 11;
       lcd.print("                     ");
       lcd.setCursor(0, 0);
       lcd.print("0123456789 __min");
        lcd.setCursor(10, 0);
        lcd.write((byte)2);
      }
    }

   else if (dialogCount == 1) {
      String timeStr = "";
      if (lcdCursorPos > -1 && lcdCursorPos < 11) {
         if (lcdCursorPos == 10) {
          timeInSeconds = 0;
          writingSlot = 11;
          lcd.setCursor(11, 0);
          lcd.print("_");
         } else {
         
          lcd.setCursor(writingSlot, 0);
          lcd.print(timeStr + String(lcdCursorPos));
          timeStr = String(lcdCursorPos);
          writingSlot += 1;
          if (writingSlot == 13) {
         timeInSeconds += lcdCursorPos * 60 ;
         wholeTime = timeInSeconds;
           dialogCount += 1;
            lcd.print("                     ");
       lcd.setCursor(0, 0);
       lcd.print("Door Shut? Yes ");
       lcd.setCursor(15, 0);
       lcd.write((byte)1);
       Serial.print(timeInSeconds);
          } else {
             timeInSeconds += lcdCursorPos * 60 * 10;
          }
          
         }
      }
    }
  else if (dialogCount == 2) {
    //start timer
    //TODO Turn Servo
   servo1.write(135);
    
    if (lcdCursorPos == 15) {
     lcd.setCursor(0, 0);
       lcd.print("                        ");
        lcd.setCursor(0, 1);
         lcd.print("Hold for TimeCut");
         delay(1000);
     while (timeInSeconds > 0) {
 int buttonState = digitalRead(switchPin);
      if (buttonState == 0) {
        //early access
        delay(1000);
            lcd.setCursor(0, 0);
       lcd.print("                        ");
        lcd.setCursor(0, 1);
         lcd.print("                      ");

         int randomC1 = rand() % 9 + -9;
          int randomC2 = rand() % 9 + -9;
        int randomX1 = rand() % 9 + -9;
          int randomX2 = rand() % 9 + -9;

//         randomX1 = 1;
//         randomX2 = 1;
      //(randomx1*X + randomC1)(randomx2*X + randomC2)
        int aInt = randomX1*randomX2;
        String aString = String(aInt);

      
        aString += "x^2";
        
        int bInt = (randomC2 * randomX1) + (randomC1 * randomX2);
        
        String bString = String(bInt);
        if (bInt > -1) {
          bString = "+" + bString;
        }
        bString += "x";

        int cInt = (randomC2 * randomC1);
        String cString = String(cInt);
         if (cInt > -1) {
          cString = "+" + cString;
        }
        answer = randomC1 * randomC2 * randomX1 * randomX2;
//        answer1 = randomC1;
//        answer2 = randomC2;
      String problem = aString + bString + cString;
      lcd.setCursor(0, 0);
       lcd.print(problem);
          lcd.setCursor(0, 1);
         lcd.print("Hold When Ready");

         delay(1000);
       while (digitalRead(switchPin) != 0) { }
     
     lcd.setCursor(0, 0);
     answerGiven = 0;
       lcd.print("-0123456789 ____");
       lcd.setCursor(0, 1);
       lcd.print("                      ");
       dialogCount += 1;
       writingSlot = 12;
       return;
      }
      timeInSeconds -=1;
       String strToDisplay = "";
       int minutes = (int)timeInSeconds / 60;
       int remainderSeconds = timeInSeconds % 60;
       
       if (minutes > 59) {
        int hours = minutes/60;
        minutes = minutes % 60;

         strToDisplay += hours;
         strToDisplay += ":";
        
       } 
       if (minutes > 9) {
        strToDisplay += String(minutes);
       } else {
         strToDisplay += "0";
         strToDisplay += String(minutes);
       }
        strToDisplay += ":";
        if (remainderSeconds > 9) {
        strToDisplay += String(remainderSeconds);
        } else {
          strToDisplay += "0";
            strToDisplay += String(remainderSeconds);
        
        
       } 
      
      lcd.setCursor(0, 0);
      lcd.print(strToDisplay);
      delay(1000);
    }
       servo1.write(90);
        resetLCD();
        dialogCount = 0;
           analogWrite(11,254);
      delay(3000);
       analogWrite(11,0);
    //finish timer
   }
   
   } else if (dialogCount == 3) {

    String timeStr = "";
   
      if (lcdCursorPos > -1 && lcdCursorPos < 11) {
         if (lcdCursorPos == 0) {
         neg = 1;
        Serial.print("neg \t");
        Serial.print(neg);
          lcd.setCursor(11, 0);
          lcd.print("-");
         } else {
         
          lcd.setCursor(writingSlot, 0);
          lcd.print(timeStr + String(lcdCursorPos - 1));
          timeStr = String(lcdCursorPos - 1);
          writingSlot += 1;
          if (writingSlot == 16) {
         answerGiven += lcdCursorPos - 1 * 1 ;
         if (neg == 1) {
          answerGiven = answerGiven * -1;
         }
            Serial.print("AnswerGiven: \n");
            Serial.print(answerGiven);
            
            neg = 0;
              lcd.print("                          ");
            if(answerGiven == answer) {
              //correct - time
               dialogCount = 2;
              timeInSeconds -= wholeTime/timeCutPercentage;
               lcd.setCursor(0, 0);
             
               
                    lcd.print("-" + String((wholeTime/timeCutPercentage)/60) + "min Continue");
                     lcd.setCursor(15, 0);
                     lcd.write((byte)1);
      
              
            } else if (answerGiven != answer) {
            dialogCount = 2;
            lcd.setCursor(0, 0);
      
       lcd.print("Wrong! Continue");
        lcd.setCursor(15, 0);
                     lcd.write((byte)1);
        
       
            }
          } else if (writingSlot == 13) {
             answerGiven += (lcdCursorPos - 1) * 1000;
              Serial.print("AnswerGiven: \n");
            Serial.print(lcdCursorPos - 1);
          } else if (writingSlot == 14) {
             answerGiven += (lcdCursorPos - 1) * 100;
              Serial.print("AnswerGiven: \n");
            Serial.print(lcdCursorPos - 1);
          } else if (writingSlot == 15) {
             answerGiven += (lcdCursorPos - 1) * 10;
              Serial.print("AnswerGiven: \n");
            Serial.print(lcdCursorPos - 1);
          }
          
         }
      }
      
   }
    
  } 
  } else {
    buttonPressLock = false;
  }
  digitalWrite(ledPin, ledState); // Turn on/off light when pressing the button
  int X = analogRead(pinX); // Read the X axis analog value
  int Y = analogRead(pinY); // Read the Y axis analog value
// Serial.print(X);
// Serial.print("\t");
// Serial.print(Y);
// Serial.print("\n");
  int dir = 0;
  lcd.setCursor(0, 0);
if(analogRead(pinX) > 508 && analogRead(pinX) < 520){
//lcd.print("STOPPED");
} else if(analogRead(pinX) < 400) {
//lcd.print("REVERSE");
dir += 419;
} else if(analogRead(pinX) > 600) {
//lcd.print("FORWARD");
dir += 547;
} else {
  lcd.print("");
}

lcd.setCursor(0, 1);
if(analogRead(pinY) > 508 && analogRead(pinY) < 520){
//lcd.print("STOPPED");
} else if(analogRead(pinY) < 400) {
////lcd.print("LEFT ");
dir += 947;
} else if(analogRead(pinY) > 600) {
//lcd.print("RIGHT");
dir += 1087;
} else {
  lcd.print("");
}
int selPin = 0;
if (dir == 947) {
  //right
////  Serial.print("Right");
  if (lcdCursorPos != 15) {
  lcdCursorPos += 1;
  selPin = pinX;
   
  }
}
if (dir == 1087) {
  //left
   // Serial.print("Left");
    if (lcdCursorPos != 0) {
   lcdCursorPos -= 1;
     selPin = pinY;
   
    
   
    }
}

//Serial.print(dir);
//Serial.print("\n");
if (oldPos != lcdCursorPos) {
lcd.setCursor(oldPos, 1);
  lcd.write(" ");  
  
lcd.setCursor(lcdCursorPos, 1);
  lcd.write((byte)0);  
  oldPos = lcdCursorPos;
  delay(200);
}

}
